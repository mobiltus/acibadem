﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Acibadem.Models;
using System.DirectoryServices.Protocols;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using System.Configuration;

namespace Acibadem.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
       
        public ActionResult Login()
        {
            return this.View();
        }
        [HttpPost]
        [Obsolete]
        public ActionResult Login(LoginModel Model, EventArgs e)
        {
            try
            {
                string dominName = string.Empty;
                string adPath = string.Empty;
                string userName = Model.UserName.Trim().ToUpper();
                string txtPassword = Model.Password.Trim();
                string strError = string.Empty;
                try
                {
                    foreach (string key in ConfigurationSettings.AppSettings.Keys)
                    {
                        dominName = key.Contains("DirectoryDomain") ?
                       ConfigurationSettings.AppSettings[key] : dominName;
                        adPath = key.Contains("DirectoryPath") ?
                       ConfigurationSettings.AppSettings[key] : adPath;
                        if (!String.IsNullOrEmpty(dominName) && !String.IsNullOrEmpty(adPath))
                        {
                            if (true == AuthenticateUser(dominName, userName, txtPassword, adPath,
                           out strError))
                            {
                                Session["User"] = userName;
                                return View();
                            }
                            dominName = string.Empty;
                            adPath = string.Empty;
                            if (String.IsNullOrEmpty(strError)) break;
                        }

                    }
                    if (!string.IsNullOrEmpty(strError))
                    {
                        strError = "Hatalı Kullanıcı Adı veya Şifre !";
                    }
                }
                catch
                {

                }
                finally
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }
        public ActionResult LogOff() {
            return View();
        }
     
        public bool AuthenticateUser(string domain, string username,string password, string LdapPath, out string Errmsg)
        {
            Errmsg = "";
            string domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(LdapPath, domainAndUsername, password);
            try
            {

                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();
                if (null == result)
                {
                    return false;
                }

                LdapPath = result.Path;
                string _filterAttribute = (String)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                Errmsg = ex.Message;
                return false;
                throw new Exception("Error authenticating user." + ex.Message);
            }
            return true;
        }
    }
}