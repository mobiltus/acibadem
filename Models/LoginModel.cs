﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Acibadem.Models
{
    public class LoginModel
    {

        [Required]
        [Display(Name = "User name")]
        public String UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

       
    }
}